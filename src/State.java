public class State {
    public String[] coins = new String[3];

    State() {
        coins[0] = "";
        coins[1] = "";
        coins[2] = "";
    }

    State(String coin0, String coin1, String coin2) {
        coins[0] = coin0;
        coins[1] = coin1;
        coins[2] = coin2;
    }

    public String getCoin(int index) {
        return coins[index];
    }

    public void setCoin(int index, String str) {
        coins[index] = str;
    }

    public boolean checkWin(){
        if (coins[0].equals("T") && coins[1].equals("H") && coins[2].equals("T"))
            return true;
        else
            return false;
    }

}
